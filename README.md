# ETSMobile-Ionic

Ce projet est une nouvelle version multiplateforme (iOS, Android) d'ÉTSMobile utilisant Ionic Framework. 

L'objectif, non seulement à visées éducatives, est de proposer une alternative aux applications natives. À plus long terme, un remplacement éventuel sur les stores permettrait de simplifier et centraliser les processus de développement et de déploiement des mises à jour.

## Get Started
### Installation de Ionic

Ionic requiert Node.JS, disponible à l'adresse suivante : https://nodejs.org/en/
Nous allons, dans un premier temps, l'installer sur notre machine afin d'exécuter ensuite la commande suivante :

```bash
npm install -g cordova ionic
```

Cette commande utilise le gestionnaire de dépendance npm de Node.JS pour installer le framework Ionic sur notre machine.
(Le -g signifie que l'installation est globale au système, les programmes installés ainsi sont effectivement disponibles dans le bash, à l'instar de apt-get sur Linux)

### Exécuter le projet
On peut exécuter le projet sur différentes plateformes : Android, iOS, Navigateur...
Pour exécuter le projet dans un navigateur, on utilise la commande suivante : 

```bash
ionic serve
```
Notons que le fichier ionic.config.json contient la configuration du proxy pour le navigateur. Effectivement, les Cross-Origin Requests (CORS) ne sont souvent pas acceptées dans un navigateur pour des raisons de sécurité, on utilise donc un proxy pour outrepasser les contraintes de sécurité liées au nom de domaine.

Pour une exécution sur un device, il n'y a pas ce problème et il faut penser à retirer la configuration du proxy dans le code source.

Si on veut exécuter le code source dans un émulateur ou un device physique, il faut dans un premier temps installer le SDK de la plateforme concernée. Pour compiler sur iOS, il faut utiliser un Mac.

Une fois le SDK accessible dans le PATH, on utilise la commande suivante :

```bash
ionic cordova run android
```

### Configuration de l'IDE

Visual Studio Code est un très bon IDE pour développer des applications Ionic. Il permet notamment de mettre des breakpoints dans le code source et a de nombreuses fonctionnalités comme l'auto-complétion, quick fix, git, recherche etc.. et est extensible via le menu des extensions (Shortcut = ctrl + shift + x).

Voici quelques extensions utiles : 
- Cordova Tools : Pour le debugging et le run intégré
- vscode-icons : Améliore la lisibilité de l'IDE en ajoutant des icônes dans l'explorateur de fichiers 
- IntelliJ IDEA Keybindings : Si vous êtes habitués aux raccourcis JetBrains

Lorsque l'on a installé l'extension Cordova Tools, on peut exécuter le projet depuis vscode. Pour une exécution sur un device via vscode, il faut par contre avoir déjà l'application au premier plan sur le device, donc avoir exécuté au préalable "ionic cordova run android" dans un terminal. Ce n'est effectivement pas un processus optimal mais ça nous permet de debugger avec des breakpoints.

## Outils utilisés
### PouchDB

La documentation de l'outil est disponible à l'adresse suivante : https://pouchdb.com/

Ces articles sont très pertinents pour comprendre les mécanismes de PouchDB :

- [12 pro tips for better code with PouchDB](https://pouchdb.com/2014/06/17/12-pro-tips-for-better-code-with-pouchdb.html)
- [Secondary indexes have landed in PouchDB](https://pouchdb.com/2014/05/01/secondary-indexes-have-landed-in-pouchdb.html)
- [Pagination strategies with PouchDB](https://pouchdb.com/2014/04/14/pagination-strategies-with-pouchdb.html)
- [Efficiently managing UI state with PouchDB](https://pouchdb.com/2015/02/28/efficiently-managing-ui-state-in-pouchdb.html)

### RxJS

La documentation de l'outil est disponible à l'adresse suivante : http://reactivex.io/rxjs/

Toutefois, ce dépôt explique bien mieux les mécanismes de RxJS avec des exemples concrets et testables en ligne :

[Learn RxJS](https://github.com/btroncone/learn-rxjs)
