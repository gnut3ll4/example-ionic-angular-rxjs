import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { BottinWebService } from "../../providers/bottin-web-service";
import { Observable } from "rxjs/Observable";
import { Employee } from "../../models/bottin/employee";
import { BottinDatabaseService } from "../../providers/bottin-database-service";

@IonicPage()
@Component({
  selector: 'page-bottin',
  templateUrl: 'bottin.html',
  providers: [BottinWebService, BottinDatabaseService]
})
export class BottinPage {
  loadedEmployees: any;

  employees: Employee[] = []
  showSpinner: boolean = false

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private webService: BottinWebService,
    private database: BottinDatabaseService) { }

  ionViewDidLoad() {

    let recherche: Observable<any> = this.webService.recherche()
      .flatMap(entries => Observable.from(entries))
      .map(employee => new Employee(employee))
      .delayWhen(employee => Observable.fromPromise(this.database.put(employee)))
      .toArray()

    this.showSpinner = true

    this.database.getEmployees()
      .flatMap(employees => Observable.if(() => employees.length == 0, recherche, Observable.of(employees)))
      .subscribe(
      employees => {
        this.loadedEmployees = employees
        this.setEmployees(employees)
      },
      err => {
        console.log(err);
      },
      () => { })

    recherche
      .subscribe(employees => {
        this.loadedEmployees = employees
      },
      err => {
        console.log(err);
      },
      () => {
        this.showSpinner = false
      })


  }



  setEmployees(employees: Employee[]) {

    // Sorting By Session in descendant order 
    this.employees =
      employees.sort(
        (employee1, employee2) => {

          let employeeName1 = employee1.Prenom + ' ' + employee1.Nom
          let employeeName2 = employee2.Prenom + ' ' + employee2.Nom

          return employeeName1.toLowerCase()
            .localeCompare(employeeName2.toLowerCase())

        })

  }

  initializeItems() {
    this.setEmployees(this.loadedEmployees)
  }

  getItems(searchbar) {
    // Reset items back to all of the items
    this.initializeItems();

    // set q to the value of the searchbar
    var q = searchbar.srcElement.value;


    // if the value is an empty string don't filter the items
    if (!q) {
      return;
    }

    this.employees = this.employees.filter((v) => {

      if (!v.Nom || !v.Prenom)
        return

      let name = (v.Prenom + ' ' + v.Nom).toLowerCase()

      q = q.toLowerCase()

      if (name.indexOf(q) > -1)
        return true;
      return false;
    });


  }


}
