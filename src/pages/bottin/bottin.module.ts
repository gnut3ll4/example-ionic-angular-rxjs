import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BottinPage } from './bottin';

@NgModule({
  declarations: [
    BottinPage,
  ],
  imports: [
    IonicPageModule.forChild(BottinPage),
  ],
  exports: [
    BottinPage
  ]
})
export class BottinPageModule {}
