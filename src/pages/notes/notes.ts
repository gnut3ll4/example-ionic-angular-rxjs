import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { CoursDatabaseService } from "../../providers/cours-database-service";
import { AuthService } from "../../providers/auth-service";
import { SignetsWebService } from "../../providers/signets-web-service";
import { Observable } from "rxjs/Observable";
import { Cours } from "../../models/cours/cours";
import { SessionCours } from "../../models/cours/session-cours";

@IonicPage()
@Component({
  selector: 'page-notes',
  templateUrl: 'notes.html',
  providers: [SignetsWebService, CoursDatabaseService, AuthService]
})
export class NotesPage {
  coursesBySession: SessionCours[];

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private signetsService: SignetsWebService,
    private coursService: CoursDatabaseService,
    private authService: AuthService, ) {
  }

  ionViewDidLoad() {

    //Load courses from API, Insert in DB and Refresh view
    this.authService.getCredentials()
      .flatMap(credentials => this.signetsService.listeCours(credentials.login, credentials.password))
      .flatMap(courses => Observable.from(courses))
      .map(course => new Cours(course))
      //Sync courses in DB
      .delayWhen(course => Observable.fromPromise(this.coursService.put(course)))
      .toArray()
      .subscribe(
      courses => {
        this.setCourses(courses)
      },
      err => {
        console.log(err);
      },
      () => console.log('Courses Synced with DB')
      )

    //Load courses from DB and Refresh view
    this.coursService.getCours()
      .subscribe(
      courses => {
        this.setCourses(courses)
      },
      err => {
        console.log(err);
      },
      () => console.log('Courses loaded from DB')
      )


  }

  setCourses(courses: Cours[]) {

    //Regrouping courses by session in a Map
    let coursesBySessionMap = new Map<string, Cours[]>()
    for (let course of courses) {
      let coursesBuffer: Cours[] = []
      if (coursesBySessionMap.has(course.session)) {
        coursesBuffer = coursesBySessionMap.get(course.session)
      }
      coursesBuffer.push(course)
      coursesBySessionMap.set(course.session, coursesBuffer)
    }

    //Converting Map to Array
    let coursesBySession: SessionCours[] = new Array<SessionCours>();
    coursesBySessionMap.forEach(
      (courses, session) =>
        coursesBySession.push(new SessionCours(session, courses))
    )

    // Sorting By Session in descendant order 
    this.coursesBySession =
      coursesBySession.sort(
        (sessionCours1, sessionCours2) => {

          // 1:Hiver  <  2:Été  <  3:Automne
          let sessionOrder = "HÉA"

          //First comparison on year
          let year1 = +sessionCours1.session.substring(1)
          let year2 = +sessionCours2.session.substring(1)
          if (year1 - year2 != 0) return year1 - year2

          //Second comparison on Session
          let session1 = sessionOrder.indexOf(sessionCours1.session.charAt(0))
          let session2 = sessionOrder.indexOf(sessionCours2.session.charAt(0))
          return session1 - session2

        }).reverse()

  }

}
