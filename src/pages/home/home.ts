import { Component } from '@angular/core';
import { NavController, IonicPage } from 'ionic-angular';
import { SignetsWebService } from '../../providers/signets-web-service';
import { StudentService } from '../../providers/db-service';
import { CoursDatabaseService } from '../../providers/cours-database-service';
import { AuthService } from '../../providers/auth-service'
import { Events } from 'ionic-angular';
import { Observable } from "rxjs";
import { ApiAppletsWebService } from "../../providers/api-applets-web-service";
import { BottinWebService } from "../../providers/bottin-web-service";


@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
  providers: [SignetsWebService, StudentService, CoursDatabaseService, AuthService, ApiAppletsWebService, BottinWebService]
})
export class HomePage {

  items = [];


  constructor(
    public navCtrl: NavController,
    private signetsService: SignetsWebService,
    private dbService: StudentService,
    private coursService: CoursDatabaseService,
    private authService: AuthService,
    private apiAppletsService: ApiAppletsWebService,
    private bottinService: BottinWebService,
    private events: Events) {

    this.authService.isLoggedIn()
      .subscribe(isLoggedIn => {
        this.events.publish('check_logged_in', isLoggedIn);
      })



  }


}
