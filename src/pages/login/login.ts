import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController, Loading, MenuController } from 'ionic-angular';
import { AuthService } from '../../providers/auth-service';

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  loading: Loading;
  credentials = { login: '', password: '' };

  constructor(public nav: NavController,
    public navParams: NavParams,
    public auth: AuthService,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    menu: MenuController
  ) { 

    menu.swipeEnable(false);
  }

  public login() {
    this.showLoading()
    this.auth.login(this.credentials)
      .subscribe(allowed => {
        if (allowed) {
          this.nav.setRoot('HomePage');
        } else {
          this.showError("Access Denied");
        }
      },
      error => {
        this.showError(error);
      });
  }

  showLoading() {
    this.loading = this.loadingCtrl.create({
      content: 'Please wait...',
      dismissOnPageChange: true
    });
    this.loading.present();
  }

  showError(text) {
    this.loading.dismiss();

    let alert = this.alertCtrl.create({
      title: 'Fail',
      subTitle: text,
      buttons: ['OK']
    });
    alert.present(prompt);
  }

}
