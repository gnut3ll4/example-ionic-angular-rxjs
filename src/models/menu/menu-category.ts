export class MenuCategory {
    title: string;
    active: boolean;

    constructor(
        in_title: string,
    ) {
        this.title = in_title;
        this.active = false;
    }

    setActive(bool:boolean){
        this.active = bool;
    }
}