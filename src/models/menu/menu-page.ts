export class MenuPage {
    title: string;
    icon: string;
    category: string;
    requiresLogin: boolean;
    component: string;

    constructor(
        in_title: string,
        in_category: string,
        in_requires_login: boolean,
        in_icon: string,
        in_component: string
    ) {
        this.title = in_title;
        this.category = in_category;
        this.requiresLogin = in_requires_login;
        this.icon = in_icon;
        this.component = in_component;
    }
}