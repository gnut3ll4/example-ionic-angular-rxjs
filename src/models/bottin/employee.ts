export class Employee {

    _id: string
    Id: number
    Nom: string
    Prenom: string
    TelBureau: string
    Emplacement: string
    Courriel: string
    Service: string
    Titre: string
    DateModif: Date
    __type:string

    constructor(fields: Partial<Employee>) {
        Object.assign(this, fields);
        this._id = 'employee_' + this.Id
    }

}

