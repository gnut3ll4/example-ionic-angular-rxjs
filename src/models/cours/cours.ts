export class Cours {

    _id: string
    sigle: string
    groupe: string
    session: string
    programmeEtudes: string
    cote: string
    nbCredits: number
    titreCours: string

    constructor(fields:Partial<Cours>
    ) {
        Object.assign(this, fields);

        this._id = 'course_'+this.session+this.sigle+this.groupe

    }


}


