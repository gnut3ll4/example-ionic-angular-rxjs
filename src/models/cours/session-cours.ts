import { Cours } from "./cours";

export class SessionCours {


    session: string
    sessionFullName: string
    courses: Cours[]


    constructor(session: string, courses: Cours[]) {

        this.session = session
        this.courses = courses

        switch (session.charAt(0)) {
            case 'A':
                this.sessionFullName = "Automne " + session.substring(1)
                break
            case 'É':
                this.sessionFullName = "Été "  + session.substring(1)
                break

            case 'H':
                this.sessionFullName = "Hiver "  + session.substring(1)
                break
            default:
                this.sessionFullName = session
                break
        }

    }


}


