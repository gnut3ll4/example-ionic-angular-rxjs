import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import PouchDB from 'pouchdb';
import 'rxjs/add/observable/from';
import { Cours } from "../models/cours/cours";

@Injectable()
export class CoursDatabaseService {

    db: any;

    constructor() {
        this.db = new PouchDB('etsmobile');
    }

    getCours(): Observable<any> {
        return Observable.fromPromise<any>(this.db.allDocs(
            {
                startkey: 'course_',
                endkey: 'course_\uffff',
                include_docs: true
            }))
            .flatMap(data => Observable.from<any>(data.rows))
            .map(row => row.doc)
            .toArray()

    }

    add(cours: Cours): Promise<any> {
        return this.db.put(cours);
    }


    public get(id: string): Promise<any> {
        return this.db.get(id);
    }

    public put(course: Cours): Promise<any> {
        var document: any = course
        return this.get(course._id).then(result => {
            document._rev = result._rev;
            return this.db.put(document);
        }, error => {
            if (error.status == "404") {
                return this.db.put(document);
            } else {
                return new Promise((resolve, reject) => {
                    reject(error);
                });
            }
        });
    }


}