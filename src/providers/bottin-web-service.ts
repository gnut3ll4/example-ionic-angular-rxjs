import { Http, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';


export class BottinWebService {
  static get parameters() {
    return [[Http]];
  }

  constructor(private http: Http) {

  }


  // Use the first apiPath when you run the app in a browser (proxy)
  apiPath = '/api_bottin'
  //apiPath = 'http://etsmtl.ca/cmspages/webservice.asmx'

  signetsHeaders = new Headers({
    'Content-Type': 'application/json',
    'Accept': 'application/json'
  });
  options = new RequestOptions({ headers: this.signetsHeaders });

  /**
   * Returns employee data by ID
   * @param id 
   */
  getFicheData(id: string): Observable<any> {
    let url = this.apiPath + '/GetFicheData'
    return this.http
      .post(url, {
        "Id": id,
      }, this.options)
      .map(res => res.json().d)
  }

  /**
   * Returns a list of all ETS departments
   */
  getListeDepartement(): Observable<any> {
    let url = this.apiPath + '/GetListeDepartement'
    return this.http
      .post(url, {}, this.options)
      .map(res => res.json().d)
  }


  /**
   * Returns a list for persons available in the directory
   * Params can be empty for a comprehensive list
   * @param filtreNom 
   * @param filtrePrenom 
   * @param filtreServiceCode 
   */
  recherche(filtreNom?: string, filtrePrenom?: string, filtreServiceCode?: string): Observable<any> {

    if (!filtreNom) filtreNom = ""
    if (!filtrePrenom) filtrePrenom = ""
    if (!filtreServiceCode) filtreServiceCode = ""

    let url = this.apiPath + '/Recherche'
    return this.http
      .post(url, {
        "FiltreNom": filtreNom,
        "FiltrePrenom": filtrePrenom,
        "FiltreServiceCode": filtreServiceCode
      }, this.options)
      .map(res => res.json().d)
  }

}
