import { Http, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from "rxjs/Observable";


export class ApiAppletsWebService {
    static get parameters() {
        return [[Http]];
    }

    constructor(private http: Http) {

    }


    // Use the first apiPath when you run the app in a browser (proxy)
    apiPath = '/api_applets'
    //apiPath = 'https://api3.clubapplets.ca/'

    /**
     * Returns ApplETS sponsors list
     */
    getPartners(): Observable<any> {
        let url = this.apiPath + '/partners'
        return this.http
            .get(url)
            .map(res => res.json())
    }

    /**
     * Returns internet consumption data for rooms in ETS Residences
     * @param phase building number (between 1 and 4)
     * @param appartment appartment number
     */
    getInternetConsumption(phase: number, appartment: number): Observable<any> {
        let url = this.apiPath + '/cooptel?phase=' + phase + '&appt=' + appartment;
        return this.http
            .get(url)
            .map(res => res.json())
    }

    /**
     * Returns Sources for ETS related Facebook Events
     */
    getSourcesEvent(): Observable<any> {
        let url = this.apiPath + '/events/sources'
        return this.http
            .get(url)
            .map(res => res.json())
    }

    /**
     * Returns Events for a given source
     * @param source 
     */
    getEvents(source: string): Observable<any> {
        let url = this.apiPath + '/events/list/' + source
        return this.http
            .get(url)
            .map(res => res.json())
    }

    /**
     * Returns Sources for ETS related Facebook post
     */
    getSourcesNews(): Observable<any> {
        let url = this.apiPath + '/news/sources'
        return this.http
            .get(url)
            .map(res => res.json())
    }

    /**
     * Returns News for a given source
     * @param source 
     */
    getNews(source: string): Observable<any> {
        let url = this.apiPath + '/news/list/' + source
        return this.http
            .get(url)
            .map(res => res.json())
    }

    /**
     * Returns events available in ETS university calendar
     * @param startDate 
     * @param endDate 
     */
    getETSCalendar(startDate: Date, endDate: Date): Observable<any> {
        let url = this.apiPath + '/calendar/ets/' +
            startDate.toISOString().substr(0, 10) + '/' +
            endDate.toISOString().substr(0, 10)
        return this.http
            .get(url)
            .map(res => res.json())
    }

}
