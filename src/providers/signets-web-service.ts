import { Http, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';


export class SignetsWebService {
  static get parameters() {
    return [[Http]];
  }

  constructor(private http: Http) {

  }


  // Use the first apiPath when you run the app in a browser (proxy)
  apiPath = '/api_signets'
  //apiPath = 'https://signets-ens.etsmtl.ca/Secure/WebServices/SignetsMobile.asmx'

  signetsHeaders = new Headers({
    'Content-Type': 'application/json',
    'Accept': 'application/json'
  });
  options = new RequestOptions({ headers: this.signetsHeaders });

  /**
   * Test Endpoint for Signets Web API
   */
  getHelloWorld(): Observable<any> {
    let url = this.apiPath + '/HelloWorld'
    return this.http
      .post(url, {}, this.options)
      .map(res => res.json())
  }

  /**
   * Retourne 'true' si le code d'accès universel et le mot de passe sont valides dans AD des étudiants
   * @param codeAccesUniversel 
   * @param motPasse 
   */
  donneesAuthentificationValides(codeAccesUniversel: string, motPasse: string): Observable<any> {
    let url = this.apiPath + '/donneesAuthentificationValides'
    return this.http
      .post(url, {
        "codeAccesUniversel": codeAccesUniversel,
        "motPasse": motPasse
      }, this.options)
      .map(res => res.json().d)
  }

  /**
   * Liste de tous les cours de l'étudiant: sigle, groupe, session, programme, cote finale, nombre de crédits et titre du cours, triée par session et sigle.
   * @param codeAccesUniversel 
   * @param motPasse 
   */
  listeCours(codeAccesUniversel: string, motPasse: string): Observable<any> {
    let url = this.apiPath + '/listeCours'
    return this.http
      .post(url, {
        "codeAccesUniversel": codeAccesUniversel,
        "motPasse": motPasse
      }, this.options)
      .map(res => {
        let result = res.json().d
        if (result.erreur.length > 0) {
          Observable.throw(result.erreur)
        } else {
          return result.liste
        }
      })
  }

  /**
   * Liste des horaires des examens finaux, avec les locaux personnels de l'étudiant
   * @param codeAccesUniversel 
   * @param motPasse 
   * @param pSession [HAÉ]yyyy (exemple : É2014)
   */
  listeHoraireExamensFin(codeAccesUniversel: string, motPasse: string, pSession: string): Observable<any> {
    let url = this.apiPath + '/listeHoraireExamensFin'
    return this.http
      .post(url, {
        "codeAccesUniversel": codeAccesUniversel,
        "motPasse": motPasse,
        "pSession": pSession
      }, this.options)
      .map(res => {
        let result = res.json().d
        if (result.erreur.length > 0) {
          Observable.throw(result.erreur)
        } else {
          return result.listeHoraire
        }
      })
  }

  /**
   * Information de base sur l'étudiant: nom, prénom, code permanent, solde
   * @param codeAccesUniversel 
   * @param motPasse 
   */
  infoEtudiant(codeAccesUniversel: string, motPasse: string): Observable<any> {
    let url = this.apiPath + '/infoEtudiant'
    return this.http
      .post(url, {
        "codeAccesUniversel": codeAccesUniversel,
        "motPasse": motPasse
      }, this.options)
      .map(res => {
        let result = res.json().d
        if (result.erreur.length > 0) {
          Observable.throw(result.erreur)
        } else {
          return result
        }
      })
  }


  /**
   * Liste des évaluations de cours effectués en ligne seulement.
   * @param codeAccesUniversel 
   * @param motPasse 
   * @param pSession 
   */
  lireEvaluationCours(codeAccesUniversel: string, motPasse: string, pSession: string): Observable<any> {
    let url = this.apiPath + '/lireEvaluationCours'
    return this.http
      .post(url, {
        "codeAccesUniversel": codeAccesUniversel,
        "motPasse": motPasse,
        "pSession": pSession
      }, this.options)
      .map(res => {
        let result = res.json().d
        if (result.erreur.length > 0) {
          Observable.throw(result.erreur)
        } else {
          return result.listeEvaluations
        }
      })
  }


  /**
   * Liste des cours pour le trimestre et le sigle de cours partiel passés en paramètres. Tous les CTN1 à l'hiver 2012, par exemple
   * @param prefixeSigleCours 3-6 letters (ex : MAT or MAT4)
   * @param pSession 
   */
  lireHoraire(prefixeSigleCours: string, pSession: string): Observable<any> {
    let url = this.apiPath + '/lireHoraire'
    return this.http
      .post(url, {
        "prefixeSigleCours": prefixeSigleCours,
        "pSession": pSession
      }, this.options)
      .map(res => {
        let result = res.json().d
        if (result.erreur.length > 0) {
          Observable.throw(result.erreur)
        } else {
          return result.listeCours
        }
      })
  }


  /**
   * Liste des séances d'activités pour l'étudiant et session voulus, pour le cours-groupe(MAT145 - 01) passé 
   * en paramètre (ou pour tous les cours si vide) et pour l'intervalle de dates demandé (toutes les séances 
   * si pas de date)
   * @param codeAccesUniversel 
   * @param motPasse 
   * @param pSession 
   * @param pCoursGroupe [optional]
   * @param pDateDebut [optional]
   * @param pDateFin [optional]
   */
  lireHoraireDesSeances(codeAccesUniversel: string, motPasse: string, pSession: string, pCoursGroupe?: string, pDateDebut?: Date, pDateFin?: Date): Observable<any> {

    let url = this.apiPath + '/lireHoraireDesSeances'
    let dateDebutParam = pDateDebut ? pDateDebut.toISOString().substr(0, 10) : ""
    let dateFinParam = pDateFin ? pDateFin.toISOString().substr(0, 10) : ""

    return this.http
      .post(url, {
        "codeAccesUniversel": codeAccesUniversel,
        "motPasse": motPasse,
        "pSession": pSession,
        "pCoursGroupe": pCoursGroupe ? pCoursGroupe : "",
        "pDateDebut": dateDebutParam,
        "pDateFin": dateFinParam
      }, this.options)
      .map(res => {
        let result = res.json().d
        if (result.erreur.length > 0) {
          Observable.throw(result.erreur)
        } else {
          return result.ListeDesSeances
        }
      })

  }


  /**
   * Liste des jours qui en remplacent d'autres, par exemple, les cours du lundi 8 octobre sont donnés le mercredi 21 novembre.
   * @param pSession (ex : É2016, A2015, H2017)
   */
  lireJoursRemplaces(pSession: string): Observable<any> {

    let url = this.apiPath + '/lireJoursRemplaces'
    return this.http
      .post(url, {
        "pSession": pSession
      }, this.options)
      .map(res => {
        let result = res.json().d
        if (result.erreur.length > 0) {
          Observable.throw(result.erreur)
        } else {
          return result.listeJours
        }
      })

  }


  /**
   * Liste de coéquipiers de l'étudiant pour le cours-groupe et l'élément d'évaluation passés en paramêtre: nom, prénom et courriel
   * @param codeAccesUniversel 
   * @param motPasse 
   * @param pSigle 
   * @param pGroupe 
   * @param pSession 
   * @param pNomElementEval 
   */
  listeCoequipiers(codeAccesUniversel: string, motPasse: string, pSigle: string, pGroupe: string, pSession: string, pNomElementEval: string): Observable<any> {
    let url = this.apiPath + '/listeCoequipiers'
    return this.http
      .post(url, {
        "codeAccesUniversel": codeAccesUniversel,
        "motPasse": motPasse,
        "pSigle": pSigle,
        "pGroupe": pGroupe,
        "pSession": pSession,
        "pNomElementEval": pNomElementEval
      }, this.options)
      .map(res => {
        let result = res.json().d
        if (result.erreur.length > 0) {
          Observable.throw(result.erreur)
        } else {
          return result.liste
        }
      })


  }


  /**
   * Liste des cours de l'étudiant entre deux sessions : sigle, groupe, session, programme, cote finale, 
   * nombre de crédits et titre du cours, triée par session et sigle.
   * @param codeAccesUniversel 
   * @param motPasse 
   * @param SesDebut 
   * @param SesFin 
   */
  listeCoursIntervalleSessions(codeAccesUniversel: string, motPasse: string, SesDebut: string, SesFin: string): Observable<any> {

    let url = this.apiPath + '/listeCoursIntervalleSessions'
    return this.http
      .post(url, {
        "codeAccesUniversel": codeAccesUniversel,
        "motPasse": motPasse,
        "SesDebut": SesDebut,
        "SesFin": SesFin

      }, this.options)
      .map(res => {
        let result = res.json().d
        if (result.erreur.length > 0) {
          Observable.throw(result.erreur)
        } else {
          return result.liste
        }
      })
  }


  /**
   * Liste des éléments d'évaluation (devoirs, labos, examens, etc.) avec la note obtenue et les statisques, comme dans SIGNETS
   * @param codeAccesUniversel 
   * @param motPasse 
   * @param pSigle 
   * @param pGroupe 
   * @param pSession 
   */
  listeElementsEvaluation(codeAccesUniversel: string, motPasse: string, pSigle: string, pGroupe: string, pSession: string): Observable<any> {

    let url = this.apiPath + '/listeElementsEvaluation'
    return this.http
      .post(url, {
        "codeAccesUniversel": codeAccesUniversel,
        "motPasse": motPasse,
        "pSigle": pSigle,
        "pGroupe": pGroupe,
        "pSession": pSession

      }, this.options)
      .map(res => {
        let result = res.json().d
        if (result.erreur.length > 0) {
          Observable.throw(result.erreur)
        } else {
          return result
        }
      })

  }


  /**
   * Liste de activités (cours, TP, Lab, etc) avec leur horaire et leur local, ainsi que les enseignants
   * @param codeAccesUniversel 
   * @param motPasse 
   * @param pSession 
   */
  listeHoraireEtProf(codeAccesUniversel: string, motPasse: string, pSession: string): Observable<any> {

    let url = this.apiPath + '/listeHoraireEtProf'
    return this.http
      .post(url, {
        "codeAccesUniversel": codeAccesUniversel,
        "motPasse": motPasse,
        "pSession": pSession

      }, this.options)
      .map(res => {
        let result = res.json().d
        if (result.erreur.length > 0) {
          Observable.throw(result.erreur)
        } else {
          return result.listeActivites
        }
      })

  }


  /**
   * Liste des programmes d'études de l'étudiant: code, libellé, moyenne, crédits réussis, etc.)
   * @param codeAccesUniversel 
   * @param motPasse 
   */
  listeProgrammes(codeAccesUniversel: string, motPasse: string): Observable<any> {

    let url = this.apiPath + '/listeProgrammes'
    return this.http
      .post(url, {
        "codeAccesUniversel": codeAccesUniversel,
        "motPasse": motPasse
      }, this.options)
      .map(res => {
        let result = res.json().d
        if (result.erreur.length > 0) {
          Observable.throw(result.erreur)
        } else {
          return result.liste
        }
      })

  }


  /**
   * Liste de toutes les sessions où l'étudiant a été actif à l'ÉTS, en version courte (A2011) et longue (Automne 2011)
   * @param codeAccesUniversel 
   * @param motPasse 
   */
  listeSessions(codeAccesUniversel: string, motPasse: string): Observable<any> {
    let url = this.apiPath + '/listeSessions'
    return this.http
      .post(url, {
        "codeAccesUniversel": codeAccesUniversel,
        "motPasse": motPasse
      }, this.options)
      .map(res => {
        let result = res.json().d
        if (result.erreur.length > 0) {
          Observable.throw(result.erreur)
        } else {
          return result.liste
        }
      })
  }


}
