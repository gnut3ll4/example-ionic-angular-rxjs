import { Http, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';


export class MoodleWebService {
    token: any;
    static get parameters() {
        return [[Http]];
    }

    constructor(private http: Http) {

    }


    // Use the first apiPath when you run the app in a browser (proxy)
    apiPath = '/api_moodle'
    //apiPath = 'https://ena.etsmtl.ca'

    /**
     * Get Moodle authentication token
     * @param username 
     * @param password 
     */
    private getToken(username: string, password: string): Observable<any> {

        let url = this.apiPath + '/login/token.php?username=' + username +
            '&password=' + password + '&service=moodle_mobile_app'
        if (!this.token) {

            this.token = this.http
                .get(url)
                .map(res => res.json())
                //caching the token
                .publishReplay(1)
                .refCount()
        }
        return this.token
    }


    /**
     * Get Moodle site info
     * @param token 
     */
    public getSiteInfo(username: string, password: string): Observable<any> {

        return this.getToken(username, password)
            .flatMap(result => {
                let url = this.apiPath + '/webservice/rest/server.php?moodlewsrestformat=json&wstoken=' + result.token +
                    '&wsfunction=moodle_webservice_get_siteinfo'

                return this.http
                    .get(url)
                    .map(res => res.json())
            })

    }

    /**
     * Get user Moodle courses
     * @param token 
     * @param userid 
     */
    public getCourses(username: string, password: string): Observable<any> {

        return this.getSiteInfo(username, password)
            .flatMap(siteInfo => this.getToken(username, password)
                .flatMap(result => {

                    let url = this.apiPath + '/webservice/rest/server.php?moodlewsrestformat=json&wstoken=' + result.token +
                        '&wsfunction=moodle_enrol_get_users_courses&userid=' + siteInfo.userid

                    return this.http.get(url)
                        .map(res => res.json())
                }))

    }

    /**
     * Get content for a giver course
     * @param token 
     * @param courseId 
     */
    getCourseContent(username: string, password: string, courseId: number): Observable<any> {

        return this.getToken(username, password)
            .flatMap(result => {

                let url = this.apiPath + '/webservice/rest/server.php?moodlewsrestformat=json&wstoken=' + result.token +
                    '&wsfunction=core_course_get_contents&courseid=' + courseId

                return this.http
                    .get(url)
                    .map(res => res.json())
            })


    }

    /**
     * Get Moodle Assignments for a given course
     * @param username 
     * @param password 
     * @param courseId 
     */
    getAssignments(username: string, password: string, courseId: number): Observable<any> {

        return this.getToken(username, password)
            .flatMap(result => {

                let url = this.apiPath + '/webservice/rest/server.php?moodlewsrestformat=json&wstoken=' + result.token +
                    '&wsfunction=mod_assign_get_assignments&courseids[]=' + courseId

                return this.http
                    .get(url)
                    .map(res => res.json())
            })
            .map(result => result.courses[0].assignments)

    }

}
