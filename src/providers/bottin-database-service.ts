import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import PouchDB from 'pouchdb';
import 'rxjs/add/observable/from';
import { Employee } from "../models/bottin/employee";

@Injectable()
export class BottinDatabaseService {

    db: any;

    constructor() {
        this.db = new PouchDB('etsmobile');
    }

    getEmployees(): Observable<any> {
        return Observable.fromPromise<any>(this.db.allDocs(
            {
                startkey: 'employee_',
                endkey: 'employee_\uffff',
                include_docs: true
            }))
            .flatMap(data => Observable.from<any>(data.rows))
            .map(row => row.doc)
            .toArray()

    }


    public get(id: string): Promise<any> {
        return this.db.get(id);
    }

    public put(employee: Employee): Promise<any> {
        delete employee.__type
        var document: any = employee
        return this.get(employee._id).then(result => {
            document._rev = result._rev;
            return this.db.put(document);
        }, error => {
            if (error.status == "404") {
                return this.db.put(document);
            } else {
                return new Promise((resolve, reject) => {
                    reject(error);
                });
            }
        });
    }


}