import { Storage } from '@ionic/storage';
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { SignetsWebService } from './signets-web-service';
import 'rxjs/add/operator/map';
import { Observable } from "rxjs/Rx";
import { Credentials } from "../models/credentials";
import * as PouchDB from 'pouchdb';


@Injectable()
export class AuthService {
  storage: Storage;
  signetsService: SignetsWebService;

  constructor(
    signetsService: SignetsWebService,
    storage: Storage
  ) {
    this.signetsService = signetsService
    this.storage = storage
  }

  public login(credentials): Observable<any> {

    if (credentials.login === null || credentials.password === null) {
      return Observable.throw("Please insert credentials");
    } else {

      return this.signetsService
        .donneesAuthentificationValides(credentials.login, credentials.password)
        .map(allowed => {
          if (allowed) {
            this.storage.set('username', credentials.login)
            this.storage.set('password', credentials.password)
          }
          return allowed
        })
    }
  }

  public getCredentials(): Observable<Credentials> {

    return Observable.zip(
      Observable.fromPromise(this.storage.get('username')),
      Observable.fromPromise(this.storage.get('password')),
      (username, password) => {
        if (!username || !password) {
          return Observable.throw("Please insert credentials")
        }

        let credentials: Credentials = new Credentials(username, password);
        return credentials;
      });
  }

  public isLoggedIn(): Observable<any> {
    return this.getCredentials()
      .map(credentials => {
        if (!credentials.login || !credentials.password) {
          return false;
        } else {
          return true;
        }
      });
  }


  public logout(): Observable<any> {
    return Observable.fromPromise(this.storage.clear())
      .map(() => {
        Observable.fromPromise(new PouchDB('etsmobile').destroy())
      })
  }

}
