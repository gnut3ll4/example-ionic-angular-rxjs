import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

// Models
import { MenuPage } from '../models/menu/menu-page';
import { MenuCategory } from '../models/menu/menu-category';
import { AuthService } from "../providers/auth-service";

import { Events } from 'ionic-angular';


@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: string = 'HomePage';

  pages: Array<MenuPage>;
  categories: Array<MenuCategory>;
  isLoggedIn: any;

  constructor(public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen, public authService: AuthService, public events: Events) {
    this.initializeApp();

    // Menu dividers
    this.categories = [
      new MenuCategory('Moi'),
      new MenuCategory('ÉTS'),
      new MenuCategory('ApplETS')
    ];

    // Menu pages
    this.pages = [
      new MenuPage('Ajourd\'hui', 'Moi', true, 'time', 'TodayPage'),
      new MenuPage('Horaire', 'Moi', true, 'calendar', 'CalendarPage'),
      new MenuPage('Notes', 'Moi', true, 'school', 'NotesPage'),
      new MenuPage('Nouvelles', 'ÉTS', false, 'paper', 'NewsPage'),
      new MenuPage('Événements', 'ÉTS', false, 'beer', 'EventsPage'),
      new MenuPage('Bottin', 'ÉTS', false, 'people', 'BottinPage')
    ];

    events.subscribe('check_logged_in', isLoggedIn => {
      this.isLoggedIn = isLoggedIn
    })
  }

  logOut() {
    this.authService.logout()
      .subscribe(() => {
        this.nav.setRoot('HomePage');
      });
  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  openPage(page: MenuPage) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }

  login() {
    //TODO push and pop for LoginPage : User may cancel the login process
    this.nav.setRoot('LoginPage')
  }

  toggleDetails(category: MenuCategory) {
    if (category.active) {
      category.setActive(false);
    } else {
      category.setActive(true);
    }
  }
}
